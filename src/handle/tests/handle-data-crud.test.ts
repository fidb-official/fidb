import { expect, test } from "vitest"
import { prepareTestServer } from "./prepareTestServer"

test("handle-data-crud", async ({ meta }) => {
  const { url, authorization } = await prepareTestServer(meta)

  const created = await (
    await fetch(new URL(`users/xieyuheng`, url), {
      method: "POST",
      headers: {
        authorization,
        "content-type": "application/json",
      },
      body: JSON.stringify({
        username: "xieyuheng",
        name: "Xie Yuheng",
      }),
    })
  ).json()

  expect(created.name).toEqual("Xie Yuheng")

  {
    const response = await fetch(new URL(`users/xieyuheng`, url), {
      method: "GET",
      headers: {
        authorization,
      },
    })

    expect(await response.json()).toEqual(created)
  }

  const putted = await (
    await fetch(new URL(`users/xieyuheng`, url), {
      method: "PUT",
      headers: {
        authorization,
        "content-type": "application/json",
      },
      body: JSON.stringify({
        "@revision": created["@revision"],
        name: "谢宇恒",
      }),
    })
  ).json()

  expect(putted.username).toEqual(undefined)
  expect(putted.name).toEqual("谢宇恒")
  expect(
    await (
      await fetch(new URL(`users/xieyuheng`, url), {
        method: "GET",
        headers: {
          authorization,
        },
      })
    ).json(),
  ).toEqual(putted)

  const patched = await (
    await fetch(new URL(`users/xieyuheng`, url), {
      method: "PATCH",
      headers: {
        authorization,
        "content-type": "application/json",
      },
      body: JSON.stringify({
        "@revision": putted["@revision"],
        username: "xyh",
      }),
    })
  ).json()

  expect(patched.username).toEqual("xyh")
  expect(patched.name).toEqual("谢宇恒")
  expect(
    await (
      await fetch(new URL(`users/xieyuheng`, url), {
        method: "GET",
        headers: {
          authorization,
        },
      })
    ).json(),
  ).toEqual(patched)

  await fetch(new URL(`users/xieyuheng`, url), {
    method: "DELETE",
    headers: {
      authorization,
    },
    body: JSON.stringify({
      "@revision": patched["@revision"],
    }),
  })

  expect(
    (
      await fetch(new URL(`users/xieyuheng`, url), {
        method: "GET",
        headers: {
          authorization,
        },
      })
    ).status,
  ).toEqual(404)
})
