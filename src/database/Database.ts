import type { DatabaseConfig } from "./DatabaseConfig"

export type Database = {
  path: string
  config: DatabaseConfig
}
