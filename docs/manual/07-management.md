---
title: Management
---

There is an online FiDB database manager
at [fidb.app/manager](https://fidb.app/manager),
which can manager data files in the form of table
(inspired by the notorious Excel).

To use it one need a link to FiDB HTTP server
and an admin token.

TODO
