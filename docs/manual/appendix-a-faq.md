---
title: FAQ
---

- **Q:** Why this manual is composed of problems and solutions?

  **A:** I learned it from Pieter Hintjens' unfinished book [Scalable C](https://readonly.link/books/https://books.readonly.link/scalable-c/book.json), I admire the author's ideas.

- **Q:** Why this manual uses examples instead of rigorous specification?

  **A:** I learned this from [The Nine Chapters on the Mathematical Art](https://en.wikipedia.org/wiki/The_Nine_Chapters_on_the_Mathematical_Art), in which algorithms are purely described by examples. I feel it is really practical and easy to understand.

  While there is no intention to be deliberately vague.
  If you find any vague part of this manual,
  that makes it hard for code that implements FiDB to work together,
  welcome to [open an issue](https://github.com/fidb-official/fidb/issues).

- **Q:** What tool is used to generate this manual from Markdown files?

  **A:** [Readonly.Link](https://readonly.link/manuals/https://readonly.link/contents/manual/en.json).

- **Q:** Who uses FiDB?

  **A:** Open source projects that uses FiDB:

  - [pomodoro](https://github.com/xieyuheng/pomodoro)
  - [mimor](https://github.com/mimor-official/mimor)
